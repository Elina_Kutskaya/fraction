package ru.eka.fraction;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс для работы с дробями
 */
public class Demo {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\ru\\eka\\fraction\\input.txt"));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\ru\\eka\\fraction\\output.txt"));
        Rational rational = new Rational();
        Rational rational1 = new Rational();
        String str;


        while ((str = bufferedReader.readLine()) != null) {
            if (!isCheck(str)) {
            }
            bufferedWriter.write(String.valueOf(actions(split(str), rational, rational1)) + "\n");
        }
        bufferedReader.close();
        bufferedWriter.close();
    }

    /**
     * Метод разбивает строку на дробь, знак операции и ещё одну дробь и передаёт
     * её в виде массива.
     *
     * @return массив
     */
    private static String[] split(String str) {
        String[] string = str.split(" ");
        return string;
    }

    /**
     * Метод разбивает первую дробь
     *
     * @param str
     * @param rational
     */
    private static void splitFirst(String[] str, Rational rational) {
        String[] first = str[0].split("/");
        rational.setNum(Integer.parseInt(first[0]));
        rational.setDenum(Integer.parseInt(first[1]));
    }

    /**
     * Метод разбивает вторую дробь
     *
     * @param str
     * @param rational1
     */
    private static void splitSecond(String[] str, Rational rational1) {
        String[] second = str[2].split("/");
        rational1.setNum(Integer.parseInt(second[0]));
        rational1.setDenum(Integer.parseInt(second[1]));
    }

    /**
     * Метод вызывает методы для работы с дробями при вводе определенного
     * знака действия
     *
     * @param str
     * @param rational
     * @param rational1
     * @return результат действия
     */
    private static Rational actions(String[] str, Rational rational, Rational rational1) {
        Rational rational2 = null;
        splitFirst(str, rational);
        splitSecond(str, rational1);
        switch (str[1]) {
            case "+":
                rational2 = rational.add(rational1);
                System.out.println(str[0] + " " + str[1] + " " + str[2] + " = " + rational2);
                break;
            case "-":
                rational2 = rational.dif(rational1);
                System.out.println(str[0] + " " + str[1] + " " + str[2] + " = " + rational2);
                break;
            case "*":
                rational2 = rational.mul(rational1);
                System.out.println(str[0] + " " + str[1] + " " + str[2] + " = " + rational2);
                break;
            case ":":
                rational2 = rational.div(rational1);
                System.out.println(str[0] + " " + str[1] + " " + str[2] + " = " + rational2);
                break;
        }
        return rational2;
    }

    /**
     * Метод проверки корректности ввода
     *
     * @param str
     * @return
     */
    public static boolean isCheck(String str) {
        Pattern pattern = Pattern.compile("^ - ?[1-9][0-9]*[/][1-9][0-9]*\\s[+-:*]\\s-?[1-9][0-9]*[/][1-9][0-9]*$");
        Matcher matcher = pattern.matcher(str);
        return matcher.find();
    }
}
