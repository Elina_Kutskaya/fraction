package ru.eka.fraction;

/**
 * Класс для демонстрации действий с дробями
 *
 * @author Куцкая Э.А., 15ОИТ18
 */
public class Rational {
    private int num;
    private int denum;

    public Rational(int num, int denum) {
        if(denum == 0) {
            throw new IllegalArgumentException("Знаменатель = 0!");
        }
        this.num = num;
        this.denum = denum;
    }

    public Rational() {
        this(1, 1);
    }

    public Rational(int num) {
        this.num = num;
        this.denum = 1;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void setDenum(int denum) {
        this.denum = denum;
    }

    @Override
    public String toString() {
        return num + " / " + denum;
    }

    /**
     * Метод для сложения двух дробей
     *
     * @return объект(дробь)
     */
    Rational add(Rational rational1) {
        int x;
        int y;
        if (this.denum == rational1.denum) {
            x = (this.num + rational1.num);
            y = denum;
        } else {
            x = (rational1.num * this.denum + this.num * rational1.denum);
            y = this.denum * rational1.denum;
        }

        return new Rational(x, y);
    }

    /**
     * Метод для вычитания двух дробей
     *
     * @return объект(дробь)
     */
    Rational dif(Rational rational1) {
        int x;
        int y;
        if (this.denum == rational1.denum) {
            x = (this.num - rational1.num);
            y = denum;
        } else {
            x = (this.num * rational1.denum - rational1.num * this.denum);
            y = (this.denum * rational1.denum);
        }

        return new Rational(x, y);
    }

    /**
     * Метод для умножения двух дробей
     *
     * @return объект(дробь)
     */
    Rational mul(Rational rational1) {
        return new Rational(this.num * rational1.num, rational1.denum * this.denum);
    }

    /**
     * Метод для деления двух дробей
     *
     * @return объект(дробь)
     */
    Rational div(Rational rational1) {
        return new Rational(this.num * rational1.denum, this.denum * rational1.num);
    }
}

